package uesocc.edu.sv.ingenieria.prn335.boundary;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PrimitiveIterator;
import java.util.StringTokenizer;

/**
 *
 * @author cristian
 */
public class Utilidades {

    /**
     *
     * @param marca es un string que describe la marca
     * @param modelo es un string que describe la modelo
     * @param color es un string que describe la color
     * @param anio es un string que describe la anio
     * @param tipo es un string que describe la tipo
     * @return devuelve un string con todos los parametros pero con su primera
     * letra en mayúscula
     */
    public String textualizar(String marca, String modelo, String color, String anio, String tipo) {
        String devuelve;
        devuelve = anio + " ";
        devuelve += marca.substring(0, 1).toUpperCase() + marca.substring(1) + " ";
        devuelve += modelo.substring(0, 1).toUpperCase() + modelo.substring(1) + " ";
        devuelve += tipo.substring(0, 1).toUpperCase() + tipo.substring(1) + " ";
        devuelve += color.substring(0, 1).toUpperCase() + color.substring(1);
        return devuelve;
    }

    /**
     * Este método sirve para que no haya dobles espacios.
     *
     * @param texto Cadena de texto que tenga una descripción de viajes.
     * @return El texto que se ha recibido con cada palabra sin dobles espacios.
     */
    public String correctorTexto(String texto) {
        if (texto != null && !texto.trim().isEmpty()) {
            String[] words = texto.split(" ");
            System.out.println("El ancho del vector es: " + words.length);
            if (words.length > 0) {
                texto = "";
                for (String word : words) {
                    if (word.trim().length() > 0) {
                        System.out.println("Esto es word---->" + word);
//                        texto += String.valueOf(word.charAt(0)).toUpperCase() + word.substring(1)+" ";
                        texto += word + " ";
                        System.out.println("texto es:" + texto);
                    } else {
                    }

                }
                return texto.substring(0, texto.length() - 1);
            }
        }
        return "";
    }

    /**
     *
     * @param texto Cadena de texto que contenga una descripción.
     * @return El número total de palabras que contenga el texto
     */
    public int contardorPalabras(String texto) {
        int numPalabras = 0;
        StringTokenizer st = new StringTokenizer(texto);
        numPalabras = st.countTokens();
        return numPalabras;
    }

    /**
     *
     * @param texto Cadena de texto que contiene la descripción de título de un
     * viaje.
     * @return Las primeras 20 letras del texto recibido en mayúsculas.
     */
    public String getTitle(String texto) {
        if (!(texto == null)) {
            if (texto.length() >= 20) {
                texto = texto.substring(0, 20);
            }
            texto = texto.toUpperCase();
            return texto;
        }
        return null;
    }

    //----------------------------------------------------------------------------------------------
    /**
     * Este método separa en un vector todos los números
     *
     * @param cadena Cadena de texto una descripción que contengan númerosy
     * simbolos.
     * @return Un arreglo de los numeros que contiene dicha cadena
     */
    public int[] separarNumeros(String cadena) {
        String[] splitted = cadena.split("(\\+|\\-|\\*|\\/)");
        int[] numeros = new int[splitted.length];
        for (int i = 0; i < splitted.length; i++) {
            numeros[i] = Integer.parseInt(splitted[i]);
        }
        return numeros;
    }

    /**
     * Este método separa en un vector todos los simbolos
     *
     * @param cadena Cadena de texto una descripción que contengan números y
     * simbolos.
     * @return Un arreglo de los simbolos que contiene dicha cadena
     */
    public String[] separarSimbolos(String cadena) {
        List<String> pivote = new LinkedList<>(Arrays.asList(cadena.split("\\d")));
        pivote.removeIf(n -> (n.trim().equals("")));
        return Arrays.copyOf(pivote.toArray(), pivote.toArray().length, String[].class);
    }

    /**
     * Este método ejecuta la suma o resta descrita en el cuadro de texto
     *
     * @param numeros arreglo de los numeros
     * @param simbolos arreglo de los simbolos
     * @return
     */
    public int ejecutar(int numeros[], String simbolos[]) {
        PrimitiveIterator.OfInt numbers = Arrays.stream(numeros).iterator();
        Iterator<String> symbols = Arrays.asList(simbolos).iterator();
        int resultado = numbers.nextInt();
        int nextentero;
        String simbolosx;
        while (numbers.hasNext()) {
            nextentero = numbers.nextInt();
            simbolosx = symbols.next();
            if (simbolosx.equals("+")) {
                resultado += nextentero;
            } else if (simbolosx.equals("-")) {
                resultado -= nextentero;
            }
        }
        return resultado;
    }
}
