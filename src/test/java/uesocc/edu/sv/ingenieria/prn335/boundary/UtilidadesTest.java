/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.boundary;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cristian
 */
public class UtilidadesTest {
    
    public UtilidadesTest() {
    }

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }
    
     /**
     * Test of textualizar method, of class Utilidades.
     */
    @Test
    public void testTextualizar() {
        System.out.println("textualizar");
        String marca = "kia";
        String modelo = "rio";
        String color = "azul";
        String anio = "2013";
        String tipo = "deportivo";
        Utilidades instance = new Utilidades();
        String expResult = "2013 Kia Rio Deportivo Azul";
        String result = instance.textualizar(marca, modelo, color, anio, tipo);
        assertEquals(expResult, result);
    }

    /**
     * Test of correctorTexto method, of class Utilidades.
     */
    @Test
    public void testCorrectorTexto() {
        System.out.println("correctorTexto");
        String texto = "hola    que   tal, este          dia hemos       viajado     a   brazil";        
        Utilidades instance = new Utilidades();
        String expResult = "hola que tal, este dia hemos viajado a brazil";
        String result = instance.correctorTexto(texto);
        assertEquals(expResult, result);
        assertNotNull(result);
    }
    
    /**
     * Test of contarPalabras method, of class Utilidades.
     */
    @Test
    public void testContadorPalabras() {
        System.out.println("contarPalabras");
        String texto = "aqui     deberian                 haber 5        palabras";
        Utilidades instance = new Utilidades();
        int expResult = 5;
        int result = instance.contardorPalabras(texto);
        assertEquals(expResult, result);
        assertNotNull(result);
    }
    
    /**
     * Test of getTitle method, of class Utilidades.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        String texto = "Viaje realizado hacia playa San Diego en El Salvador";
        Utilidades instance = new Utilidades();
        String expResult = "VIAJE REALIZADO HACI";
        String result = instance.getTitle(texto);
        System.out.println(result);
        assertEquals(expResult, result);
        assertNotNull(result);        
    }
    
    //--------------------------------------------------------------------------------------------------------
    /**
     * Test of separarNumeros method, of class Utilidades.
     */
    @Test
    public void testSepararNumeros() {
        System.out.println("separarNumeros");
        String cadena = "22+333-4321-12345";
        Utilidades instance = new Utilidades();
        int[] expResult = new int[]{22,333,4321,12345};
        int[] result = instance.separarNumeros(cadena);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of separarSimbolos method, of class Utilidades.
     */
    @Test
    public void testSepararSimbolos() {
        System.out.println("separarSimbolos");
        String cadena = "22+333-4321-12345";
        Utilidades instance = new Utilidades();
        String[] expResult = new String[]{"+","-","-"};
        String[] result = instance.separarSimbolos(cadena);
        assertArrayEquals(expResult, result);
        
    }

    /**
     * Test of ejecutar method, of class Utilidades.
     */
    @Test
    public void testEjecutar() {
        System.out.println("ejecutar");
        int[] numeros = new int[]{22,333,4321,12345};
        String[] simbolos = new String[]{"+","-","-"};
        Utilidades instance = new Utilidades();
        int expResult = -16311;
        int result = instance.ejecutar(numeros, simbolos);
        assertEquals(expResult, result);
    }
    
}
